import { createAction, props } from "@ngrx/store";
import { Profile } from "../model/profile";
import { User } from "../model/user";

const GET_USERS = 'get users';
const GET_USERS_SUCCESS_ACTION = 'get users success';
const CREATE_USER = 'create user';
const CREATE_USER_SUCCESS_ACTION = 'create user success';
const UPDATE_USER = 'update user';
const UPDATE_USER_SUCCESS_ACTION = 'update user success';
const DELETE_USER = 'delete user';
const DELETE_USER_SUCCESS_ACTION = 'delete user success';

const GET_PROFILES = 'get profiles';
const GET_PROFILES_SUCCESS_ACTION = 'get profiles success';

export const ApiGetUsers = createAction(GET_USERS);
export const GetUsersSuccess = createAction(GET_USERS_SUCCESS_ACTION, props<{ users: User[] }>());
export const ApiCreateUser = createAction(CREATE_USER, props<{ user: User }>())
export const CreateUserSuccess = createAction(CREATE_USER_SUCCESS_ACTION, props<{ user: User }>());
export const ApiUpdateUser = createAction(UPDATE_USER, props<{ id: any, user: User }>())
export const UpdateUserSuccess = createAction(UPDATE_USER_SUCCESS_ACTION, props<{ user: User }>());
export const ApiDeleteUser = createAction(DELETE_USER, props<{ id: any, user: User }>())
export const DeleteUserSuccess = createAction(DELETE_USER_SUCCESS_ACTION, props<{ user: User }>());

export const ApiGetProfiles = createAction(GET_PROFILES);
export const GetProfilesSuccess = createAction(GET_PROFILES_SUCCESS_ACTION, props<{ profiles: Profile[] }>());
