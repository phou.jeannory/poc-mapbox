import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, mergeMap } from 'rxjs/operators';
import { Profile } from "../model/profile";
import { User } from "../model/user";
import { AppService } from "../service/app.service";

import {
  ApiCreateUser,
  ApiGetUsers,
  GetUsersSuccess,
  CreateUserSuccess,
  ApiUpdateUser,
  UpdateUserSuccess,
  ApiDeleteUser,
  DeleteUserSuccess,
  ApiGetProfiles,
  GetProfilesSuccess
} from './app-actions';

@Injectable()
export class AppEffects {

  constructor(
    private appService: AppService,
    private actions: Actions
  ) { }

  getUserEffect$ = createEffect(
    () => this.actions.pipe(
      ofType(ApiGetUsers),
      mergeMap((action) => {
        return this.appService.getUsers().pipe(
          map((users: User[]) => GetUsersSuccess({ users })))
      }))
  );

  createUseEffect$ = createEffect(
    () => this.actions.pipe(
      ofType(ApiCreateUser),
      mergeMap((action) => {
        return this.appService.createUser(action.user).pipe(
          map((user: User) => CreateUserSuccess({ user })))
      }))
  );

  updateUserEffect$ = createEffect(
    () => this.actions.pipe(
      ofType(ApiUpdateUser),
      mergeMap((action) => {
        return this.appService.updateUser(action.id, action.user).pipe(
          map((user: User) => UpdateUserSuccess({ user })))
      }))
  );

  deleteUserEffect$ = createEffect(
    () => this.actions.pipe(
      ofType(ApiDeleteUser),
      mergeMap((action => {
        const id = action.id;
        const user = action.user;
        return this.appService.deleteUser(id).pipe(
          map(() => DeleteUserSuccess({ user })))
      })))
  );

  getProfilesEffect$ = createEffect(
    () => this.actions.pipe(
      ofType(ApiGetProfiles),
      mergeMap((action) => {
        return this.appService.getProfiles().pipe(
          map((profiles: Profile[]) => GetProfilesSuccess({ profiles })))
      }))
  );

}
