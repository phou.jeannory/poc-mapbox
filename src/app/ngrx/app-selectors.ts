import { createSelector } from "@ngrx/store";
import { AppState, ProfilesState, UsersState } from "./app-reducer";

const getUsersStates = (state: AppState): any => state.UsersState;

const getUsers = createSelector(
  getUsersStates,
  (userState: UsersState) => Object.values(userState.users)
)

const getProfilesState = (state: AppState): any => state.ProfilesState;

const getProfiles = createSelector(
  getProfilesState,
  (profilesState: ProfilesState) => Object.values(profilesState.profiles)
);

const getAddresses = createSelector(
  getUsersStates,
  (userState: UsersState) => userState.users.flatMap(user => user.addresses)
)

export { getUsersStates, getUsers, getProfilesState, getProfiles, getAddresses }
