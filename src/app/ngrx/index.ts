import {
  ApiGetUsers,
  GetUsersSuccess,
  ApiCreateUser,
  CreateUserSuccess,
  ApiUpdateUser,
  UpdateUserSuccess,
  ApiDeleteUser,
  DeleteUserSuccess,
  ApiGetProfiles,
  GetProfilesSuccess,
} from './app-actions';

import { AppEffects } from './app-effect';
import { usersReducer, profilesReducer } from './app-reducer';
import { getUsersStates, getUsers, getProfilesState, getProfiles } from './app-selectors';

export const fromRoot = {
  AppEffects,
  ApiGetUsers,
  GetUsersSuccess,
  ApiCreateUser,
  CreateUserSuccess,
  ApiUpdateUser,
  UpdateUserSuccess,
  ApiDeleteUser,
  DeleteUserSuccess,
  usersReducer,
  getUsersStates,
  getUsers,
  getProfilesState,
  getProfiles,
  ApiGetProfiles,
  GetProfilesSuccess,
  profilesReducer,
};
