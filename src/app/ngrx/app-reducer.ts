import { createReducer, on } from "@ngrx/store";
import { Profile } from "../model/profile";
import { User } from "../model/user";
import {
  CreateUserSuccess,
  DeleteUserSuccess,
  GetProfilesSuccess,
  GetUsersSuccess,
  UpdateUserSuccess
} from './app-actions';

export interface AppState {
  UsersState : UsersState,
  ProfilesState : ProfilesState,
}
export interface UsersState {
  users: User[];
}

export interface ProfilesState {
  profiles: Profile[];
}

const initialUSersState: UsersState = {
  users: [],
}

const initialProfilesState: ProfilesState = {
  profiles: []
}

export const usersReducer = createReducer(
  initialUSersState,
  on(GetUsersSuccess, (state, action) => ({ users: action.users })),
  on(CreateUserSuccess, (state, action) => ({ users: state.users.concat(action.user) })),
  on(UpdateUserSuccess, (state, action) => {
    const index = state.users.findIndex(
      (user) => user.id === action.user.id
    );
    const users = [...state.users];
    users[index] = action.user;
    return {
      users: users,
    };
  }),
  on(DeleteUserSuccess, (state, action) => {
    const index = state.users.findIndex(
      (user) => user.id === action.user.id
    );
    const users = [...state.users]
    users.splice(index, 1)
    return {
      users: users,
    };
  })
)

export const profilesReducer = createReducer(
  initialProfilesState,
  on(GetProfilesSuccess, (state, action) => ({ profiles: action.profiles })),
)

