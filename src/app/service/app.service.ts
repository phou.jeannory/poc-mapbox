import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Profile } from '../model/profile';
import { TravelTime } from '../model/travel-times';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private prepath : string = 'http://localhost:8091/api/';

  constructor(private httpClient: HttpClient) { }

  getTravelTimes(): Observable<TravelTime[]> {
    return this.httpClient.get<TravelTime[]>(this.prepath + 'travel-times');
  }

  getUsers(): Observable<Array<User>> {
    return this.httpClient.get<Array<User>>(this.prepath + 'users');
  }

  createUser(user: User): Observable<User> {
    return this.httpClient.post<User>(this.prepath + 'users/', user);
  }

  updateUser(id: string, user: User): Observable<User> {
    return this.httpClient.put<User>(this.prepath + 'users/' + id, user);
  }

  deleteUser(id: string): Observable<void> {
    return this.httpClient.delete<void>(this.prepath+ 'users/' + id);
  }

  getProfiles(): Observable<Profile[]> {
    return this.httpClient.get<Profile[]>(this.prepath + 'profiles');
  }
}
