import { Address } from "./address";
import { Profile } from "./profile";

export interface User{
  id?: string;
  email: string;
  subject: string;
  lastName: string;
  firstName: string;
  phoneNumber: string;
  profiles: Profile[];
  addresses: Address[];
}
