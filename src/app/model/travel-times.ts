export interface TravelTime {
  id: string;
  sca: string;
  sector: string;
  axis: string[];
}
