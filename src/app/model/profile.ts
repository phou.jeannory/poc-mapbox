export interface Profile {
  id: string;
  name: string;
  rights: Right[];
  remark: string;
}

export interface SelectedProfile {
  id: string;
  name: string;
  rights: Right[];
  remark: string;
  isSelected: boolean;
}

export enum Right {
  ADMIN, MANAGER, USER
}
