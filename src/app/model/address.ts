export interface Address {
  id: string;
  name: string;
  longitude: number;
  latitude: number;
}

