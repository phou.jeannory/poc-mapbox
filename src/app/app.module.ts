import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { UsersComponent } from './modules/users/users/users.component';
import { UsersListComponent } from './modules/users/users-list/users-list.component';
import { UsersModalComponent } from './modules/users/users-modal/users-modal.component';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { profilesReducer, usersReducer } from './ngrx/app-reducer';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './ngrx/app-effect';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { HomeComponent } from './modules/home/home.component';
import { MapComponent } from './modules/map/map/map.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { MapNavBarComponent } from './modules/map/map-nav-bar/map-nav-bar.component';
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    UsersComponent,
    UsersListComponent,
    UsersModalComponent,
    HomeComponent,
    MapComponent,
    MapNavBarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    StoreModule.forRoot({
      UsersState: usersReducer,
      ProfilesState: profilesReducer,
    }),
    EffectsModule.forRoot([AppEffects]),
    MatDialogModule,
    ReactiveFormsModule,
    MatTableModule,
    StoreDevtoolsModule.instrument(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
