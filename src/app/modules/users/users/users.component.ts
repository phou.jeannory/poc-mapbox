import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as appActions from '../../../ngrx/app-actions';
import { UsersState } from '../../../ngrx/app-reducer';
import { MatDialog } from '@angular/material/dialog';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { UsersModalComponent } from '../users-modal/users-modal.component';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  state: UsersState | null = null;

  constructor(
    private store: Store<any>,
    public dialog: MatDialog,
  ) {
    this.store.dispatch(appActions.ApiGetUsers());
  }

  ngOnInit(): void {
    this.subscriptions.push(this.store.subscribe(state => {
      this.state = state.UsersState;
    }));
  }

  createUser() {
    let dialogRef = this.dialog.open(UsersModalComponent, {
      width: '250px'
    });
    dialogRef.afterClosed().pipe(
      filter(user => user)
    ).subscribe(user => {
      this.saveNewUser(user);
    })
  }

  saveNewUser(user: User) {
    this.store.dispatch(appActions.ApiCreateUser({user: user}));
  }

  updateUser(user: User) {
    let dialogRef = this.dialog.open(UsersModalComponent, {
      width: '250px',
      data: { user: user }
    });

    dialogRef.afterClosed().pipe(
      filter(user => user)
    ).subscribe(user => {
      this.saveUpdatedUser(user);
    })
  }

  saveUpdatedUser(user: User) {
    this.store.dispatch(appActions.ApiUpdateUser({id: user.id, user: user}));
  }

  ngOnDestroy(): void {
    console.log('unsubscribe')
    this.subscriptions.forEach(subscription => subscription.unsubscribe)
  }

}
