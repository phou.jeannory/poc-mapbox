import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { filter, Observable } from 'rxjs';
import { User } from 'src/app/model/user';
import { UsersModalComponent } from '../users-modal/users-modal.component';
import * as appActions from '../../../ngrx/app-actions';
import {MatTableDataSource} from '@angular/material/table';
import * as appSelectors from '../../../ngrx/app-selectors';
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  user$: Observable<any> = this.store.select(appSelectors.getUsers);
  users: User[] = [];
  displayedColumns: string[] = ['id', 'email','subject','lastName','firstName','phoneNumber','profiles','action',]

  dataSource = new MatTableDataSource<User[]>();
  clickedRows = new Set<any>();
  constructor(public dialog: MatDialog, private store: Store<any>,) {
    this.user$.subscribe((users) => {
      this.users = users;
      this.dataSource = users;
    })
   }

  ngOnInit(): void {
  }

  updateUser(user: User) {
    let dialogRef = this.dialog.open(UsersModalComponent, {
      width: '250px',
      data: { user: user }
    });

    dialogRef.afterClosed().pipe(
      filter(user => user)
    ).subscribe(user => {
      this.saveUpdatedUser(user);
    })
  }

  saveUpdatedUser(user: User) {
    this.store.dispatch(appActions.ApiUpdateUser({id: user.id, user: user}));
  }

  deleteUser(user: User) {
    this.store.dispatch(appActions.ApiDeleteUser({id: user.id, user: user}))
  }

}
