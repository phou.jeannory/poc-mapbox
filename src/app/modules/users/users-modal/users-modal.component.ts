import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Profile, SelectedProfile } from 'src/app/model/profile';
import { User } from 'src/app/model/user';
import * as profilesSelectors from '../../../ngrx/app-selectors';

@Component({
  selector: 'app-users-modal',
  templateUrl: './users-modal.component.html',
  styleUrls: ['./users-modal.component.css']
})
export class UsersModalComponent implements OnInit, OnDestroy {

  userForm: FormGroup;
  profiles: Profile[] = [];
  userProfiles: Profile[] = [];
  selectedProfiles: SelectedProfile[] = [];
  subscriptions: Subscription[] = [];
  profiles$: Observable<any> = this.store.select(profilesSelectors.getProfiles);

  constructor(
    public dialogRef: MatDialogRef<UsersModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private store: Store<any>,
  ) {
    this.profiles$.subscribe((profiles) => {
      this.profiles = profiles;
    })

    if (data?.user) {
      const user = data.user;
      this.userForm = this.formBuilder.group(
        {
          id: [user.id, Validators.required],
          email: [user.email, Validators.required],
          subject: [user.subject, Validators.required],
          lastName: [user.lastName, Validators.required],
          firstName: [user.firstName, Validators.required],
          phoneNumber: [user.phoneNumber, Validators.required],
        }
      )
      this.userProfiles = user.profiles
      this.profiles.forEach(profile => {
        const userProfile = this.userProfiles.find(userProfile => userProfile.name === profile.name)
        const isSelected = userProfile ? true : false;
        const selectedProfile = {
          id: profile.id,
          name: profile.name,
          rights: profile.rights,
          remark: profile.remark,
          isSelected: isSelected
        };
        this.selectedProfiles.push(selectedProfile)
      })
    } else {
      this.userForm = this.formBuilder.group(
        {
          email: ['', Validators.required],
          subject: ['', Validators.required],
          lastName: ['', Validators.required],
          firstName: ['', Validators.required],
          phoneNumber: ['', Validators.required],
        }
      )
      this.profiles.forEach(profile => {
        const selectedProfile = {
          id: profile.id,
          name: profile.name,
          rights: profile.rights,
          remark: profile.remark,
          isSelected: false
        };
        this.selectedProfiles.push(selectedProfile)
      })
    }
  }

  ngOnInit(): void {
  }

  selectProfile(id: string) {
    const index = this.selectedProfiles.findIndex(selectedProfile => selectedProfile.id === id)
    this.selectedProfiles[index].isSelected = !this.selectedProfiles[index].isSelected;
  }

  onSubmit() {
    const profiles: Profile[] = this.selectedProfiles.filter(selectedprofile => selectedprofile.isSelected === true);
    const fromData = this.userForm?.value;
    const user: User = {
      id: fromData.id ? fromData.id : null,
      email: fromData.email,
      subject: fromData.subject,
      lastName: fromData.lastName,
      firstName: fromData.firstName,
      phoneNumber: fromData.phoneNumber,
      profiles: profiles,
      addresses: []
    }
    this.dialogRef.close(user)
  }

  ngOnDestroy(): void {
    console.log('unsubscribe')
    this.subscriptions.forEach(subscription => subscription.unsubscribe)
  }

}
