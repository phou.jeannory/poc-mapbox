import { Component, Input, OnInit } from '@angular/core';
import { Address } from 'src/app/model/address';

@Component({
  selector: 'app-map-nav-bar',
  templateUrl: './map-nav-bar.component.html',
  styleUrls: ['./map-nav-bar.component.css']
})
export class MapNavBarComponent implements OnInit {
  @Input()
  addresses: Address[] = []

  constructor() { }

  ngOnInit(): void { }

}
