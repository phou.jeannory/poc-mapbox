import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapNavBarComponent } from './map-nav-bar.component';

describe('MapNavBarComponent', () => {
  let component: MapNavBarComponent;
  let fixture: ComponentFixture<MapNavBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapNavBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
