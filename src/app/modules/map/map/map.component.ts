import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { Address } from 'src/app/model/address';
import * as mapboxgl from 'mapbox-gl';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as appSelectors from '../../../ngrx/app-selectors';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  map: mapboxgl.Map | undefined;
  style = 'mapbox://styles/mapbox/streets-v11';
  lat = 48.90;
  lng = 2.35;

  addresses$: Observable<any> = this.store.select(appSelectors.getAddresses);
  addresses: Address[] = [];
  constructor(private store: Store<any>,) {
    this.addresses$.subscribe((addresses) => {
      this.addresses = addresses;
    })
  }
  // constructor(){}

  ngOnInit() {
    (mapboxgl as typeof mapboxgl).accessToken = environment.mapbox.accessToken;
      this.map = new mapboxgl.Map({
        container: 'map',
        style: this.style,
        zoom: 10,
        center: [this.lng, this.lat]

        // container: 'map',
        // style: 'mapbox://styles/mapbox/streets-v11'

        // container: "map", // container id
        // style: "mapbox://styles/mapbox/dark-v10",
        // zoom: 6,
        // center: [4.6559, 50.4997]

        // container: 'mapbox',
        // style: 'mapbox://styles/mapbox/streets-v11',
        // center: [35.1715959,31.9305916,],
        // zoom: 10

        // container: 'map',
        // style: 'mapbox://styles/mapbox/streets-v11',
        // center: [12.550343, 55.665957],
        // zoom: 8
    });

    // Create a default Marker and add it to the map.
    const marker1 = new mapboxgl.Marker()
    // .setLngLat([12.554729, 55.70651])
    .setLngLat([2.3, 48.8])
    .addTo(this.map);

    // Create a default Marker, colored black, rotated 45 degrees.
    const marker2 = new mapboxgl.Marker({ color: 'black', rotation: 45 })
    // .setLngLat([12.65147, 55.608166])
    .setLngLat([2.35, 48.85])
    .addTo(this.map);

    this.map.addControl(new mapboxgl.NavigationControl());

  }




}
